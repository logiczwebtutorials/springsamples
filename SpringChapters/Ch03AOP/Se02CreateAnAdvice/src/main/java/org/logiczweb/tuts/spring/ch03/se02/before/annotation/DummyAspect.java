package org.logiczweb.tuts.spring.ch03.se02.before.annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.logiczweb.tuts.spring.ch03.se02.PointCuts;

/**
 * Created by pranav on 3/28/2015.
 */

@Aspect
public class DummyAspect extends PointCuts {


    @Before("pointCutmethod()")//applying pointcut on before advice
    public void doBeforeMethod(JoinPoint jp)//it is advice (before advice)
    {
        System.out.println("****SPRING AOP**** doBeforeMethod : Executing before method!");
        System.out.println("additional concern");
        //System.out.println("Method Signature: "  + jp.getSignature());
    }
}
