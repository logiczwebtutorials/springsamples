package org.logiczweb.tuts.spring.ch03.se02.afterthrowing.xml;

import org.springframework.aop.ThrowsAdvice;

/**
 * Created by pranav on 3/28/2015.
 */
public class DoAfterThrowingExceptionMethod implements ThrowsAdvice {
    public void afterThrowing(IllegalArgumentException e) throws Throwable {
        System.out.println("****SPRING AOP**** DoAfterThrowingExceptionMethod : Executing when method throws exception!");
    }
}