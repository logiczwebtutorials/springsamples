package org.logiczweb.tuts.spring.ch03.se02.before.annotation;

import org.logiczweb.tuts.spring.ch03.se02.SimpleService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pranav on 3/28/2015.
 */
public class App {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        SimpleService simpleService = (SimpleService) context.getBean("simpleServiceProxy");
        simpleService.printNameId();
        System.out.println("--------------");
        try {
            simpleService.checkName();
        } catch (Exception e) {
            System.out.println("SimpleService: Method checkName() exception thrown..");
        }
        System.out.println("--------------");
        simpleService.sayHello("Spring Code Sample");
    }


}
