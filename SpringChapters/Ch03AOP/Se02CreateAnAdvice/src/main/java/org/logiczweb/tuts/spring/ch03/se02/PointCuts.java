package org.logiczweb.tuts.spring.ch03.se02;

import org.aspectj.lang.annotation.Pointcut;

/**
 * Created by pranav on 3/28/2015.
 */
public class PointCuts {
    @Pointcut("execution(* org.logiczweb.tuts.spring.ch03.se02.SimpleService.*(..))")
    public void pointCutmethod() {
        System.out.println("****SPRING AOP**** pointCutmethod : PointCut!");
    }
}
