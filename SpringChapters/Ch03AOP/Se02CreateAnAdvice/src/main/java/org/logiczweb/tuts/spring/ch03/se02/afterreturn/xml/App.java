package org.logiczweb.tuts.spring.ch03.se02.afterreturn.xml;

import org.logiczweb.tuts.spring.ch03.se02.SimpleService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by pranav on 3/28/2015.
 */
public class App {
    public static void main(String[] args) {

        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("META-INF\\config\\applicationContext.xml",
                "META-INF\\config\\afterreturn\\applicationContext.xml");
        SimpleService simpleService = (SimpleService) context.getBean("simpleServiceProxy");
        simpleService.printNameId();
        System.out.println("--------------");
        try {
            simpleService.checkName();
        } catch (Exception e) {
            System.out.println("SimpleService: Method checkName() exception thrown..");
        }
        System.out.println("--------------");
        simpleService.sayHello("Spring Code Sample");
        context.close();
    }
}
