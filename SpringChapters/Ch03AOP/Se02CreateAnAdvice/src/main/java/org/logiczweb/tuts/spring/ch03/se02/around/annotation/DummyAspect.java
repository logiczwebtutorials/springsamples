package org.logiczweb.tuts.spring.ch03.se02.around.annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.logiczweb.tuts.spring.ch03.se02.PointCuts;

import java.util.Arrays;

/**
 * Created by pranav on 3/28/2015.
 */

@Aspect
public class DummyAspect extends PointCuts {


    @Before("pointCutmethod()")//applying pointcut on before advice
    public void doBeforeMethod(JoinPoint jp)//it is advice (before advice)
    {
        System.out.println("****SPRING AOP**** doBeforeMethod : Executing before method!");
        System.out.println("additional concern");
        //System.out.println("Method Signature: "  + jp.getSignature());
    }

    @AfterReturning("pointCutmethod()")//applying pointcut on before advice
    public void doAfterReturningMethod(JoinPoint jp)//it is advice (before advice)
    {
        System.out.println("****SPRING AOP**** doAfterReturningMethod : Executing after method return!");
        System.out.println("additional concern");
        //System.out.println("Method Signature: "  + jp.getSignature());
    }

    @AfterThrowing("pointCutmethod()")//applying pointcut on before advice
    public void doAfterThrowingExceptionMethod(JoinPoint jp)//it is advice (before advice)
    {
        System.out.println("****SPRING AOP**** doAfterThrowingExceptionMethod : Executing when method throws exception!");
        System.out.println("additional concern");
        //System.out.println("Method Signature: "  + jp.getSignature());
    }

    @Around("pointCutmethod()")//applying pointcut on before advice
    public void doAroundMethod(ProceedingJoinPoint pjp) throws Throwable//it is advice (before advice)
    {
        System.out.println("****SPRING AOP**** DoAroundMethod: Method name : "
                + pjp.getSignature());
        System.out.println("****SPRING AOP**** DoAroundMethod: Method arguments : "
                + Arrays.toString(pjp.getArgs()));
        // same with MethodBeforeAdvice
        System.out.println("****SPRING AOP**** DoAroundMethod: Before method executing!");

        try {
            // proceed to original method call
            Object result = pjp.proceed();
            // same with AfterReturningAdvice
            System.out.println("****SPRING AOP**** DoAroundMethod: After method executing!");

        } catch (IllegalArgumentException e) {
            // same with ThrowsAdvice
            System.out.println("****SPRING AOP**** DoAroundMethod: When method throws Exception!");
            throw e;
        }
    }
}
