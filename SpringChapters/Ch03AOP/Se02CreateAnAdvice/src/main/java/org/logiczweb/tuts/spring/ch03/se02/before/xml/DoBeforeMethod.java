package org.logiczweb.tuts.spring.ch03.se02.before.xml;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * Created by pranav on 3/28/2015.
 */
public class DoBeforeMethod implements MethodBeforeAdvice {
    public void before(Method method, Object[] args, Object target)
            throws Throwable {
        System.out.println("****SPRING AOP**** DoBeforeMethod : Executing before method!");
    }
}