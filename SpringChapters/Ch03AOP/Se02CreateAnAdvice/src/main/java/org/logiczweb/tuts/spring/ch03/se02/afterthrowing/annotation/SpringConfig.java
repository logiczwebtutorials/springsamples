package org.logiczweb.tuts.spring.ch03.se02.afterthrowing.annotation;

import org.logiczweb.tuts.spring.ch03.se02.SimpleService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by pranav on 3/28/2015.
 */
@Configuration
@EnableAspectJAutoProxy
public class SpringConfig {
    @Bean
    public SimpleService simpleServiceProxy() {
        SimpleService simpleService = new SimpleService();
        simpleService.setName("John Doe");
        simpleService.setId(121);

        return simpleService;
    }

    @Bean
    public DummyAspect dummyAspect() {
        DummyAspect dummyAspect = new DummyAspect();

        return dummyAspect;
    }
}
