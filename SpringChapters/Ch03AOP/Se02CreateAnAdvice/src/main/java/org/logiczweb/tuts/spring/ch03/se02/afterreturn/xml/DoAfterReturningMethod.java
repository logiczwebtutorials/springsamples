package org.logiczweb.tuts.spring.ch03.se02.afterreturn.xml;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * Created by pranav on 3/28/2015.
 */
public class DoAfterReturningMethod implements AfterReturningAdvice {
    public void afterReturning(Object returnValue, Method method,
                               Object[] args, Object target) throws Throwable {
        System.out.println("****SPRING AOP**** DoAfterReturningMethod : Executing after method return!");
    }

}