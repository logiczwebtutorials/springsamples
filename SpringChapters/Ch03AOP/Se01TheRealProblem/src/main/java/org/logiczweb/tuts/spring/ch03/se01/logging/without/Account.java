package org.logiczweb.tuts.spring.ch03.se01.logging.without;

/**
 * Created by pranav on 3/27/2015.
 */
public class Account {

    public void deposit() {

        // Actual Deposit Logic comes here

    }

    public void withdraw() {

        // Actual Withdraw Logic comes here

    }

}
