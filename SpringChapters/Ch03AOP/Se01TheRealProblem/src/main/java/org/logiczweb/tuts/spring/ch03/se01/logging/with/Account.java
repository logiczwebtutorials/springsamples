package org.logiczweb.tuts.spring.ch03.se01.logging.with;

/**
 * Created by pranav on 3/27/2015.
 */
public class Account {
    public void deposit() {

        // Transaction Management
        // Logging
        // Checking for the Privileged User
        // Actual Deposit Logic comes here

    }

    public void withdraw() {

        // Transaction Management
        // Logging
        // Checking for the Privileged User
        // Actual Withdraw Logic comes here

    }

}
