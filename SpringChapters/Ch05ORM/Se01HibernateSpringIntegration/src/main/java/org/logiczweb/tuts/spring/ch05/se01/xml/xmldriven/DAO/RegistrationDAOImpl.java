package org.logiczweb.tuts.spring.ch05.se01.xml.xmldriven.DAO;

import org.logiczweb.tuts.spring.ch05.se01.DAO.RegistrationDAO;
import org.logiczweb.tuts.spring.ch05.se01.RegistrationModel;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Pranav on 29-03-2015.
 */
public class RegistrationDAOImpl extends HibernateDaoSupport implements RegistrationDAO {

    @Override
    public void save(RegistrationModel registrationModel) {
        Serializable out = getHibernateTemplate().save(registrationModel);

        if (out != null) {
            System.out.println(out);
            System.out.println("Employee saved with id=" + registrationModel.getId());
        } else System.out.println("Employee save failed with id=" + registrationModel.getId());

    }

    @Override
    public RegistrationModel getById(int id) {
        List list = getHibernateTemplate().find(
                "from RegistrationModel where id=?", id
        );
        return (RegistrationModel) list.get(0);
    }

    @Override
    public void update(RegistrationModel registrationModel) {
        getHibernateTemplate().update(registrationModel);
        System.out.println("Employee updated with id=" + registrationModel.getId());
    }

    @Override
    public void deleteById(int id) {
        List list = getHibernateTemplate().find(
                "from RegistrationModel where id=?", id
        );
        if (list != null && list.size() == 1) {
            RegistrationModel registrationModel = (RegistrationModel) list.get(0);

            if (registrationModel != null) {
                getHibernateTemplate().delete(registrationModel);
                System.out.println("Employee deleted with id=" + id);
            } else {
                System.out.println("Employee with id=" + id + " not found.");
            }
        } else {
            System.out.println("Employee with id=" + id + " not found.");
        }


    }

    @Override
    public List<RegistrationModel> getAll() {
        List<RegistrationModel> list = (List<RegistrationModel>) getHibernateTemplate().find(
                "from RegistrationModel where id!=?", 0
        );
        return list;
    }
}
