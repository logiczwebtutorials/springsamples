package org.logiczweb.tuts.spring.ch05.se01.xml.xmldriven.service;


import org.logiczweb.tuts.spring.ch05.se01.DAO.RegistrationDAO;
import org.logiczweb.tuts.spring.ch05.se01.RegistrationModel;
import org.logiczweb.tuts.spring.ch05.se01.service.RegistrationService;

import java.util.List;

/**
 * Created by Pranav on 29-03-2015.
 */
public class RegistrationServiceImpl implements RegistrationService {
    private RegistrationDAO registrationDAO;

    @Override
    public void save(RegistrationModel registrationModel) {
        registrationDAO.save(registrationModel);
    }

    @Override
    public RegistrationModel getById(int id) {
        return registrationDAO.getById(id);
    }

    @Override
    public void update(RegistrationModel registrationModel) {
        registrationDAO.update(registrationModel);
    }

    @Override
    public void deleteById(int id) {
        registrationDAO.deleteById(id);
    }

    @Override
    public List<RegistrationModel> getAll() {
        return registrationDAO.getAll();
    }

    public RegistrationDAO getRegistrationDAO() {
        return registrationDAO;
    }

    public void setRegistrationDAO(RegistrationDAO registrationDAO) {
        this.registrationDAO = registrationDAO;
    }
}
