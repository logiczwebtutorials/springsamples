package org.logiczweb.tuts.spring.ch04.se02.annotation.config;

import org.logiczweb.tuts.spring.ch04.IJDBCConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by pranav on 3/30/2015.
 */
@Profile(value = IJDBCConstants.MODE_OFFICE)
@Configuration
@PropertySource("classpath:" + IJDBCConstants.PROPERTIES_OFFICE)
public class DatabaseConfigOffice extends AbstractConfig {
}
