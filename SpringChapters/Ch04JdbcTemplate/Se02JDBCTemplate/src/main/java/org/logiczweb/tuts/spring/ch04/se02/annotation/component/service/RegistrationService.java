package org.logiczweb.tuts.spring.ch04.se02.annotation.component.service;

import org.logiczweb.tuts.spring.ch04.se02.RegistrationModel;

import java.util.List;

/**
 * Created by Pranav on 29-03-2015.
 */
public interface RegistrationService {
    //Create
    public void save(RegistrationModel registrationModel);

    //Read
    public RegistrationModel getById(int id);

    //Update
    public void update(RegistrationModel registrationModel);

    //Delete
    public void deleteById(int id);

    //Get All
    public List<RegistrationModel> getAll();
}
