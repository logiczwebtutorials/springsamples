package org.logiczweb.tuts.spring.ch04.se02.annotation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.logiczweb.tuts.spring.ch04.IJDBCConstants.MODE_HOME;
import static org.logiczweb.tuts.spring.ch04.IJDBCConstants.MODE_OFFICE;

/**
 * Created by pranav on 3/30/2015.
 */
@Configuration
@ComponentScan("org.logiczweb.tuts.spring.ch04.se02.annotation.component")
@Profile(value = {MODE_OFFICE, MODE_HOME})
public class ComponentConfiguration {
}
