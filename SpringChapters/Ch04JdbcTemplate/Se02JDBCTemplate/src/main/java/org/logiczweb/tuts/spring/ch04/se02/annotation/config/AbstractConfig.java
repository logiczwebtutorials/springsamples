package org.logiczweb.tuts.spring.ch04.se02.annotation.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import static org.logiczweb.tuts.spring.ch04.IJDBCConstants.*;

/**
 * Created by pranav on 3/30/2015.
 */

public abstract class AbstractConfig {

    @Value(value = "${" + DB_DRIVER_CLASS + "}")
    private String DRIVER_CLASS;

    @Value(value = "${" + DB_URL + "}")
    private String URL;


    @Value(value = "${" + DB_USERNAME + "}")
    private String USERNAME;

    @Value(value = "${" + DB_PASSWORD + "}")
    private String PASSWORD;

    //To resolve ${} in @Value
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(DRIVER_CLASS);
        driverManagerDataSource.setUrl(URL);
        driverManagerDataSource.setUsername(USERNAME);
        driverManagerDataSource.setPassword(PASSWORD);
        return driverManagerDataSource;
    }
}
