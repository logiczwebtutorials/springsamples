package org.logiczweb.tuts.spring.ch04.se02;

/**
 * Created by Pranav on 29-03-2015.
 */
public class RegistrationModel {

    Integer id;

    String first;

    String last;

    Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee [ \"ID\" : " + id + "," +
                "\"FIRST NAME\" : \"" + first + "\"," +
                "\"LAST NAME\" : \"" + last + "\"," +
                "\"AGE\" : " + age + ",]";
    }
}
