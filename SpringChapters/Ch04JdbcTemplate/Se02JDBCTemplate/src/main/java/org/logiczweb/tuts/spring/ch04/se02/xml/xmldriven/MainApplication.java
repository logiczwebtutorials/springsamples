package org.logiczweb.tuts.spring.ch04.se02.xml.xmldriven;

import org.logiczweb.tuts.spring.ch04.IJDBCConstants;
import org.logiczweb.tuts.spring.ch04.se02.RegistrationModel;
import org.logiczweb.tuts.spring.ch04.se02.xml.xmldriven.service.RegistrationService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;

import java.util.List;

/**
 * Created by Pranav on 29-03-2015.
 */
public class MainApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", IJDBCConstants.MODE_OFFICE);
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF\\config\\applicationContext.xml"});
        Environment environment = context.getEnvironment();
        //environment.acceptsProfiles(IJDBCConstants.MODE_OFFICE);

        RegistrationService registrationService = context.getBean("registrationService", RegistrationService.class);

        List<RegistrationModel> employees = registrationService.getAll();

        for (int i = 0; i < employees.size(); i++) {
            RegistrationModel model = employees.get(i);
            System.out.println(model);
        }


    }
}
