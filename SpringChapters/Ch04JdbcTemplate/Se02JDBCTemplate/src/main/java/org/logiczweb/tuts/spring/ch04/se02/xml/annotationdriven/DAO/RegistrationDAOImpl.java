package org.logiczweb.tuts.spring.ch04.se02.xml.annotationdriven.DAO;

import org.logiczweb.tuts.spring.ch04.se02.RegistrationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Pranav on 29-03-2015.
 */
@Repository(value = "registrationDAO")
public class RegistrationDAOImpl implements RegistrationDAO {
    @Autowired
    private DataSource dataSource;

    @Override
    public void save(RegistrationModel registrationModel) {
        String query = "INSERT INTO Registration " +
                "VALUES (?, ?, ?, ?)";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Object[] args = new Object[]{registrationModel.getId(), registrationModel.getFirst(),
                registrationModel.getLast(), registrationModel.getAge()};

        int out = jdbcTemplate.update(query, args);

        if (out != 0) {
            System.out.println("Employee saved with id=" + registrationModel.getId());
        } else System.out.println("Employee save failed with id=" + registrationModel.getId());

    }

    @Override
    public RegistrationModel getById(int id) {
        String query = "SELECT id, first, last, age FROM Registration";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //using RowMapper anonymous class, we can create a separate RowMapper for reuse
        RegistrationModel registrationModel = jdbcTemplate.queryForObject(query, new Object[]{id}, new RowMapper<RegistrationModel>() {

            @Override
            public RegistrationModel mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                RegistrationModel registrationModel = new RegistrationModel();
                registrationModel.setId(rs.getInt("id"));
                registrationModel.setFirst(rs.getString("first"));
                registrationModel.setLast(rs.getString("last"));
                registrationModel.setAge(rs.getInt("age"));
                return registrationModel;
            }
        });

        return registrationModel;
    }

    @Override
    public void update(RegistrationModel registrationModel) {

        String query = "update Registration set first=?, last=?, age=? where id=?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Object[] args = new Object[]{registrationModel.getFirst(), registrationModel.getLast(), registrationModel.getAge(), registrationModel.getId()};

        int out = jdbcTemplate.update(query, args);
        if (out != 0) {
            System.out.println("Employee updated with id=" + registrationModel.getId());
        } else System.out.println("No Employee found with id=" + registrationModel.getId());

    }

    @Override
    public void deleteById(int id) {
        String query = "delete from Registration where id=?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        int out = jdbcTemplate.update(query, id);
        if (out != 0) {
            System.out.println("Employee deleted with id=" + id);
        } else System.out.println("No Employee found with id=" + id);
    }

    @Override
    public List<RegistrationModel> getAll() {
        String query = "select ID, FIRST ,LAST ,AGE from Registration";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<RegistrationModel> registrationList = new ArrayList<RegistrationModel>();

        List<Map<String, Object>> registrationRows = jdbcTemplate.queryForList(query);

        for (Map<String, Object> empRow : registrationRows) {
            RegistrationModel registrationModel = new RegistrationModel();
            registrationModel.setId(Integer.parseInt(String.valueOf(empRow.get("id"))));
            registrationModel.setFirst(String.valueOf(empRow.get("first")));
            registrationModel.setLast(String.valueOf(empRow.get("last")));
            registrationModel.setAge(Integer.parseInt(String.valueOf(empRow.get("age"))));
            registrationList.add(registrationModel);
        }
        return registrationList;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
