package org.logiczweb.tuts.spring.ch04.se01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.logiczweb.tuts.spring.ch04.IJDBCConstants.MODE_HOME;
import static org.logiczweb.tuts.spring.ch04.se01.JDBCConnection.getDbProperties;
/**
 * Created by pranav on 3/28/2015.
 */
public class JDBCConnectionSamples {
    public static void createRegistrationTable() {
        Statement stmt = null;
        Connection connection = null;
        try {
            connection = JDBCConnection.jdbcConnectionMethod1(getDbProperties(MODE_HOME));
            System.out.println("Creating table in given database...");
            stmt = connection.createStatement();

            String sql = "CREATE TABLE REGISTRATION " +
                    "(id INTEGER not NULL, " +
                    " first VARCHAR(255), " +
                    " last VARCHAR(255), " +
                    " age INTEGER, " +
                    " PRIMARY KEY ( id ))";

            stmt.executeUpdate(sql);
            System.out.println("Created table in given database...");

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }


    public static void dropRegistrationTable() {
        Statement stmt = null;
        Connection connection = null;
        try {
            connection = JDBCConnection.jdbcConnectionMethod1(getDbProperties(MODE_HOME));
            System.out.println("DROP table in given database...");
            stmt = connection.createStatement();

            String sql = "DROP TABLE REGISTRATION";

            stmt.executeUpdate(sql);
            System.out.println("DROP table in given database...");

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }


    public static void insertRecordsTable() {
        Statement stmt = null;
        Connection connection = null;
        try {
            connection = JDBCConnection.jdbcConnectionMethod1(getDbProperties(MODE_HOME));
            System.out.println("DROP table in given database...");
            stmt = connection.createStatement();

            //STEP 4: Execute a query
            System.out.println("Inserting records into the table...");

            String sql = "INSERT INTO Registration " +
                    "VALUES (100, 'Zara', 'Ali', 18)";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO Registration " +
                    "VALUES (101, 'Mahnaz', 'Fatma', 25)";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO Registration " +
                    "VALUES (102, 'Zaid', 'Khan', 30)";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO Registration " +
                    "VALUES(103, 'Sumit', 'Mittal', 28)";
            stmt.executeUpdate(sql);
            System.out.println("Inserted records into the table...");

            stmt.executeUpdate(sql);

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }


    public static void selectRecordsFromTable() {
        Statement stmt = null;
        Connection connection = null;
        try {
            connection = JDBCConnection.jdbcConnectionMethod1(getDbProperties(MODE_HOME));
            System.out.println("DROP table in given database...");
            stmt = connection.createStatement();

            String sql = "SELECT id, first, last, age FROM Registration";
            ResultSet rs = stmt.executeQuery(sql);
            //STEP 5: Extract data from result set
            while (rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("id");
                int age = rs.getInt("age");
                String first = rs.getString("first");
                String last = rs.getString("last");

                //Display values
                System.out.print("ID: " + id);
                System.out.print(", Age: " + age);
                System.out.print(", First: " + first);
                System.out.println(", Last: " + last);
            }
            rs.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    connection.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }


    public static void main(String[] args) {
        //createRegistrationTable();
        insertRecordsTable();
        selectRecordsFromTable();
        // dropRegistrationTable();
    }
}
