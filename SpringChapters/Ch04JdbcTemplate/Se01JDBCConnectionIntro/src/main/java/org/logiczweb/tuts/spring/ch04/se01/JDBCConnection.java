package org.logiczweb.tuts.spring.ch04.se01;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static org.logiczweb.tuts.spring.ch04.IJDBCConstants.*;


/**
 * Created by pranav on 3/28/2015.
 */
public class JDBCConnection {

    public static Connection jdbcConnectionMethod1(Properties properties) {
        Connection conn = null;
        try {
            Class.forName(properties.getProperty(DB_DRIVER_CLASS));
            String URL = properties.getProperty(DB_URL);
            String USER = properties.getProperty(DB_USERNAME);
            String PASS = properties.getProperty(DB_PASSWORD);
            conn = DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: unable to load driver class!");
            System.exit(1);
        } catch (SQLException e) {
            System.out.println("Error: Sql Exception!");
            System.exit(2);
        }

        return conn;
    }

    public static Connection jdbcConnectionMethod2(Properties properties) {
        Connection conn = null;
        try {
            final String driverClass = properties.getProperty(DB_DRIVER_CLASS);
            Class<?> cls = Class.forName(driverClass);
            Driver myDriver = (Driver) cls.newInstance();

            DriverManager.registerDriver(myDriver);
            String URL = properties.getProperty(DB_URL);
            String USER = properties.getProperty(DB_USERNAME);
            String PASS = properties.getProperty(DB_PASSWORD);
            conn = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException e) {
            System.out.println("Error: Sql Exception!");
            System.exit(3);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static Properties getDbProperties(String mode) {
        Properties props = new Properties();
        InputStream fis = null;
        try {
            if (mode == MODE_HOME) {
                fis = JDBCConnection.class.getClassLoader().getResourceAsStream(PROPERTIES_HOME);
            } else if (mode == MODE_OFFICE) {
                fis = JDBCConnection.class.getClassLoader().getResourceAsStream(PROPERTIES_OFFICE);
            }
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return props;
    }

    public static void main(String args[]) {
        Connection jdbcConnectionMethod1 = jdbcConnectionMethod1(getDbProperties(MODE_HOME));
        Connection jdbcConnectionMethod2 = jdbcConnectionMethod2(getDbProperties(MODE_HOME));
        System.out.println(jdbcConnectionMethod1);
        System.out.println(jdbcConnectionMethod2);
        try {
            if (jdbcConnectionMethod1 != null)
                jdbcConnectionMethod1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (jdbcConnectionMethod2 != null)
                jdbcConnectionMethod2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
