package org.logiczweb.tuts.spring.ch02.se04;

/**
 * Created by pranav on 3/26/2015.
 */
public class A {
    B b;

    public A() {
        System.out.println("a is created");
    }

    public A(B b) {
        System.out.println("Construcor of A invoked with B");
        this.b = b;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Class A:  " + super.toString() + "  " + b;
    }
}
