package org.logiczweb.tuts.spring.ch02.se01;

/**
 * Created by pranav on 3/26/2015.
 */
public class AImpl implements A {
    @Override
    public A getA() {
        System.out.println("Method called");
        return this;
    }
}
