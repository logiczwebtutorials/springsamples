package org.logiczweb.tuts.spring.ch04;

/**
 * Created by pranav on 3/28/2015.
 */
public interface IJDBCConstants {
    String MODE_HOME = "HOME";
    String MODE_OFFICE = "OFFICE";
    String DB_DRIVER_CLASS = "DB_DRIVER_CLASS";
    String DB_URL = "DB_URL";
    String DB_USERNAME = "DB_USERNAME";
    String DB_PASSWORD = "DB_PASSWORD";

    String PROPERTIES_HOME = "META-INF\\config\\database\\local_db.properties";
    String PROPERTIES_OFFICE = "META-INF\\config\\database\\office_db.properties";
}
