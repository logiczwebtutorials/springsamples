package org.logiczweb.tuts.spring.ch02.se02.constructor;

import org.logiczweb.tuts.spring.ch02.se02.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {

        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/constructorApplicationContext.xml"});

        Employee e1 = (Employee) context.getBean("e1");
        e1.display();

        Employee e2 = (Employee) context.getBean("e2");
        e2.display();

        Employee e3 = (Employee) context.getBean("e3");
        e3.display();
    }
}
