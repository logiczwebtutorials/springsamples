package org.logiczweb.tuts.spring.ch02.se04.mo02;

import org.logiczweb.tuts.spring.ch02.se04.mo02.config.AnnotationConfig;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.byconstructor.AutowiredByConstructorService;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.byname.AutowiredByNameService;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.bytype.AutowiredByTypeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pranav on 3/27/2015.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AnnotationConfig.class);
        loadConfigurationByName(context);
        loadConfigurationByType(context);
        loadConfigurationByConstructor(context);
    }

    public static void loadConfigurationByName(ApplicationContext context) {
        AutowiredByNameService bean = context.getBean(AutowiredByNameService.class);
        bean.service();
    }


    public static void loadConfigurationByConstructor(ApplicationContext context) {
        AutowiredByConstructorService bean = context.getBean(AutowiredByConstructorService.class);
        bean.service();
    }

    public static void loadConfigurationByType(ApplicationContext context) {
        AutowiredByTypeService bean = context.getBean(AutowiredByTypeService.class);
        bean.service();
    }
}
