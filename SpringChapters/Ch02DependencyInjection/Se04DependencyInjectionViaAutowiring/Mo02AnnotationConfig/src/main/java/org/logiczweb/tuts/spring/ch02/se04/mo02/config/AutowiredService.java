package org.logiczweb.tuts.spring.ch02.se04.mo02.config;

/**
 * Created by pranav on 3/27/2015.
 */
public interface AutowiredService {
    public void service();
}
