package org.logiczweb.tuts.spring.ch02.se04.mo02.config.byname;

import org.logiczweb.tuts.spring.ch02.se04.B;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.AutowiredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by pranav on 3/27/2015.
 */
@Component
public class AutowiredByNameService implements AutowiredService {
    @Autowired
    @Qualifier(value = "b")
    B b;


    @Override
    public void service() {
        System.out.println("Autowire by name");
        System.out.println(b);
        System.out.println();
    }
}
