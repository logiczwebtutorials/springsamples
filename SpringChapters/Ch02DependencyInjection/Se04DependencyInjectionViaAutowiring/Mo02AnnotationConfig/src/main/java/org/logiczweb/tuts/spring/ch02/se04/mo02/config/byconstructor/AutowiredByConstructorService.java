package org.logiczweb.tuts.spring.ch02.se04.mo02.config.byconstructor;

import org.logiczweb.tuts.spring.ch02.se04.B;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.AutowiredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by pranav on 3/27/2015.
 */
@Component
public class AutowiredByConstructorService implements AutowiredService {

    private B b;

    //Autowired annotation on Constructor is equivalent to autowire="constructor"
    @Autowired(required = false)
    public AutowiredByConstructorService(@Qualifier("b") B b) {
        System.out.println("AutowiredByConstructorService: Constructor");
        this.b = b;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public void service() {
        System.out.println("Autowire By Constructor Service");
        System.out.println(b);
        System.out.println();
    }
}
