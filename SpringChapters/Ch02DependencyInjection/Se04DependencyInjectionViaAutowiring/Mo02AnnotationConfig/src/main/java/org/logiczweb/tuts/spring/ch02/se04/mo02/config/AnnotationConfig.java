package org.logiczweb.tuts.spring.ch02.se04.mo02.config;

import org.logiczweb.tuts.spring.ch02.se04.A;
import org.logiczweb.tuts.spring.ch02.se04.B;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pranav on 3/27/2015.
 */
@Configuration
@ComponentScan("org.logiczweb.tuts.spring.ch02.se04.mo02.config")
public class AnnotationConfig {
    @Bean
    public B b() {
        B b = new B();
        return b;
    }

    @Bean
    public B b1() {
        B b = new B();
        return b;
    }

    @Bean
    public A a() {
        A a = new A();
        return a;
    }


}
