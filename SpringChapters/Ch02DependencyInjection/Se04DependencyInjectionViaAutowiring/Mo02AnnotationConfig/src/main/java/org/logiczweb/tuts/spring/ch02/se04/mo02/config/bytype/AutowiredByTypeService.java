package org.logiczweb.tuts.spring.ch02.se04.mo02.config.bytype;

import org.logiczweb.tuts.spring.ch02.se04.A;
import org.logiczweb.tuts.spring.ch02.se04.B;
import org.logiczweb.tuts.spring.ch02.se04.mo02.config.AutowiredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by pranav on 3/27/2015.
 */
@Component
public class AutowiredByTypeService implements AutowiredService {

    //Autowired annotation on variable/setters is equivalent to autowire="byType"
    @Autowired
    @Qualifier("b")
    private B b;

    @Autowired
    private A a;


    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public void service() {
        System.out.println("Autowire by Type");
        System.out.println(a);
        System.out.println(b);
        System.out.println();
    }
}
