package org.logiczweb.tuts.spring.ch02.se03.config;

import org.logiczweb.tuts.spring.ch02.se02.Address;
import org.logiczweb.tuts.spring.ch02.se02.Employee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pranav on 3/26/2015.
 */
@Configuration
public class AnnotationConfiguration {
    @Bean
    public Employee e1() {
        Employee employee = new Employee(10);
        return employee;
    }

    @Bean
    public Employee e2() {
        Employee employee = new Employee("Joh Doe");
        return employee;
    }

    @Bean
    public Employee e3() {
        Employee employee = new Employee(10, "John Doe");
        return employee;
    }

    @Bean
    public Employee e4() {
        Employee employee = new Employee(10, "John Doe", a1());
        return employee;
    }


    @Bean
    public Employee e5() {
        Employee employee = new Employee();
        employee.setId(10);
        employee.setName("John Doe");
        employee.setAddress(a1());
        return employee;
    }

    @Bean
    public Address a1() {
        Address address = new Address("Trivandrum", "Kerala", "India");
        return address;
    }
}
